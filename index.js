const express = require("express")
const app = express()
const port = 4000
const bodyParser = require("body-parser")

app.use(bodyParser.urlencoded({extended:false}))

app.set("view engine", "ejs")

app.use(express.static("public"))



app.get('/',(req,res)=>{
    res.render("index")
})

app.get('/game',(req,res)=>{
    res.render("game")
})

app.get('/login',(req,res)=>{
    res.render("login")
})


app.post('/login',(req,res)=>{
  
    const username = req.body.username
    const password = req.body.password

    if(username && password){
        res.render("loginpage",{username,password})
    }else{
        res.render("login")
    }


    
})


app.listen(port,()=> console.log('app berjalan di port '+port))