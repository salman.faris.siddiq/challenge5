const player = document.getElementsByClassName("player-choose")
var elementResult = document.getElementById("result")
var versus = document.getElementById("versus")
playerChoose = ""
round = 0
versus.innerText = "VS"
Array.from(player).forEach(function(element){
 
    element.addEventListener("click",function(){
        if(round === 0){
            Array.from(player).forEach(function(element){
                element.classList.remove("active")
            })
            this.classList.add("active")
            playerChoose =  this.getAttribute("data-player")
            comChoose = computerChoose()
            getResult = getResultFinal(playerChoose, comChoose)
    
           
            elementResult.innerHTML = `<button>${getResult}</button>`
            versus.innerText = ""
            console.log("pilihan user",playerChoose)
            console.log("pilihan com",comChoose)
            console.log("pemenang nya :",getResult)
            round = 1
        }
     
    })
})

function computerChoose(){
    var arrComChoose = ["batu", "kertas", "gunting"]
    var random = Math.floor(Math.random() * 3);
    var comChoose = arrComChoose[random]
    var elementChoose = document.getElementById(`choose-${comChoose}`)
    elementChoose.classList.add("active")
    return (comChoose)
}


function getResultFinal(playerChoose, comChoose){
    var resultFinal = ""
    if(playerChoose === "batu" && comChoose === "batu"){
        resultFinal = "draw"
    }
    
    if(playerChoose === "kertas" && comChoose === "batu"){
        resultFinal = "player1 win"
    }
    
    if(playerChoose === "gunting" && comChoose === "batu"){
        resultFinal = "com win"
    }
    
    if(playerChoose === "batu" && comChoose === "kertas"){
        resultFinal = "com win"
    }
    
    if(playerChoose === "kertas" && comChoose === "kertas"){
        resultFinal = "draw"
    }
    
    if(playerChoose === "gunting" && comChoose === "kertas"){
        resultFinal = "player1 win"
    }
    
    if(playerChoose === "batu" && comChoose === "gunting"){
        resultFinal = "player1 win"
    }
    
    if(playerChoose === "kertas" && comChoose === "gunting"){
        resultFinal = "com win"
    }
    
    if(playerChoose === "gunting" && comChoose === "gunting"){
        resultFinal = "draw"
    }
    
    return resultFinal

}

var elementRefresh = document.getElementsByClassName("refresh")

elementRefresh[0].addEventListener("click",function(){
    Array.from(player).forEach(function(element){
        element.classList.remove("active")
    })
    elementResult.innerText = ""
    versus.innerText = "VS"
    round = 0
})


